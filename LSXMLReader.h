//
//  LSXMLReader.h
//  
//
//  Created by Joshua Howland on 6/18/13.
//
//

#import <UIKit/UIKit.h>

@interface LSXMLReader : NSObject

@property (nonatomic, strong) NSMutableArray *dictionaryStack;
@property (nonatomic, strong) NSMutableString *textInProgress;
@property (nonatomic, assign) NSError *__autoreleasing *errorPointer;

+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)errorPointer;
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)errorPointer;

@end
